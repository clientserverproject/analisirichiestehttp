package it.unibas.analisirichieste.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/*
 * @author https://github.com/vincenzopalazzo
 */
public class HttpServletAnlisi extends HttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpServletAnlisi.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //Logic applicative
        String path = super.getServletContext().getRealPath("/WEB-INF/classes/it/unibas/analisirichieste/servlet/HttpServletAnlisi.class");
        LOGGER.debug("Directory esecution application: " + path);
        File file = new File(path);
        if (!file.exists()) {
            LOGGER.debug("File not exist");
        }
        Long lastModified = file.lastModified();
        LOGGER.debug("Last modific page: " + lastModified);

        Date date = new Date(lastModified);
        DateFormat formatter = DateFormat.getDateInstance(DateFormat.LONG);
        String dataString = formatter.format(date);
        LOGGER.debug("La data di ultima modifica e': " + dataString);
        
        //Generate a respose
        HttpSession sessione = req.getSession();
        if (sessione == null) {
            LOGGER.debug("Sessione nulla");
        }

        resp.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");
        resp.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        resp.addHeader("Cache-Control", "post-check=0, pre-check=0");
        resp.addHeader("Pragma", "no-cache");
        resp.addHeader("Pragma", "no-cache");
        resp.setContentType("text/html;");

        String cssTag = "<link rel='stylesheet' type='text/css' href='/analisi_richieste_http/css/style.css' />";

        PrintWriter writer = resp.getWriter();
        writer.println("<html>");
        writer.println("<head>");
        writer.println("<title> Your Request info</title>");
        writer.println(cssTag);
        writer.println("</head>");
        writer.println("<body>");
        writer.println("<br/>");
        writer.println("<table>");
        writer.println("<tr>");
        writer.println("<th  class='presentation'>Request Inf</th>");
        writer.println("<th  class='presentation'>Your Request Inf</th>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Host </td>");
        writer.println("<td  class='information'>" + req.getRequestURL() + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> User Agent </td>");
        writer.println("<td  class='information'>" + req.getHeader("user-agent") + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Accept </td>");
        writer.println("<td  class='information'>" + req.getHeader("accept") + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Accept Language </td>");
        writer.println("<td  class='information'>" + req.getHeader("accept-language") + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Accept Econding </td>");
        writer.println("<td  class='information'>" + req.getHeader("accept-encoding") + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Accept Charset </td>");
        writer.println("<td  class='information'>" + req.getHeader("charset") + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Keep Alive </td>");
        writer.println("<td  class='information'>" + req.getHeader("keep-alive") + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Connection </td>");
        writer.println("<td  class='information'>" + req.getHeader("connection") + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Cookies </td>");
        writer.println("<td  class='information'>" + req.getCookies() + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> If Modified Since </td>");
        writer.println("<td  class='information'>" + req.getHeader("if-modified-since") + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Cache control</td>");
        writer.println("<td  class='information'>" + req.getHeader("cache-control") + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Cache control</td>");
        writer.println("<td  class='information'>" + req.getHeader("cache-control") + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> IP</td>");
        writer.println("<td  class='information'>" + req.getRemoteAddr() + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Host</td>");
        writer.println("<td  class='information'>" + req.getRemoteHost() + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Method</td>");
        writer.println("<td  class='information'>" + req.getMethod() + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Protocol</td>");
        writer.println("<td  class='information'>" + req.getProtocol() + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Server name</td>");
        writer.println("<td  class='information'>" + req.getServerName() + " </td>");
        writer.println("</tr>");
        writer.println("<tr>");
        writer.println("<td  class='presentation'> Server name</td>");
        writer.println("<td  class='information'>" + req.getServerPort() + " </td>");
        writer.println("</tr>");
        writer.println("</table>");
        writer.println("<br/>");
        writer.println("<br/>");
        writer.println("<p id='last_change'>");
        writer.println("Last change in date " + dataString + " from Vincent Palazzo");
        writer.println("</p>");
        writer.println("</body>");
        writer.println("</html>");

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

}
